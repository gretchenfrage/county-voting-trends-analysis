

use std::{
    fs::File,
    collections::{
        BTreeMap,
        btree_map::Entry,
    },
};
use serde::{Deserialize, de::DeserializeOwned};

//,votes_dem,votes_gop,total_votes,per_dem,per_gop,diff,per_point_diff,state_abbr,county_name,combined_fips


#[derive(Debug, Clone, Deserialize)]
struct Record2016 {
    index: usize,
    votes_dem: f64,
    votes_gop: f64,
    total_votes: f64,
    per_dem: f64,
    per_gop: f64,
    diff: String,
    per_point_diff: String,
    state_abbr: String,
    county_name: String,
    combined_fips: i64,
}

#[derive(Debug, Clone, Deserialize)]
struct Record2020 {
    state_name: String,
    county_fips: String,
    county_name: String,
    votes_gop: i64,
    votes_dem: i64,
    total_votes: i64,
    diff: f64,
    per_gop: f64,
    per_dem: f64,
    per_point_diff: f64,
}

#[derive(Debug, Clone, Deserialize)]
struct Record {
    state: String,
    county: String,
    votes_dem: i64,
    votes_rep: i64,
}

impl From<Record2016> for Record {
    fn from(record: Record2016) -> Self {
        assert_eq!(record.votes_dem % 1.0, 0.0, "Fractional number of votes");
        assert_eq!(record.votes_gop % 1.0, 0.0, "Fractional number of votes");
        Record {
            state: abbreviation_to_state(&record.state_abbr).into(),
            county: record.county_name,
            votes_dem: record.votes_dem as i64,
            votes_rep: record.votes_gop as i64,
        }
    }
}

impl From<Record2020> for Record {
    fn from(record: Record2020) -> Self {
        Record {
            state: record.state_name,
            county: record.county_name,
            votes_dem: record.votes_dem,
            votes_rep: record.votes_gop,
        }
    }
}

fn abbreviation_to_state(abbreviation: &str) -> &'static str {
    match abbreviation {
        "AL" => "Alabama",
        "AK" => "Alaska",
        "AZ" => "Arizona",
        "AR" => "Arkansas",
        "CA" => "California",
        "CO" => "Colorado",
        "CT" => "Connecticut",
        "DE" => "Delaware",
        "FL" => "Florida",
        "GA" => "Georgia",
        "HI" => "Hawaii",
        "ID" => "Idaho",
        "IL" => "Illinois",
        "IN" => "Indiana",
        "IA" => "Iowa",
        "KS" => "Kansas",
        "KY" => "Kentucky",
        "LA" => "Louisiana",
        "ME" => "Maine",
        "MD" => "Maryland",
        "MA" => "Massachusetts",
        "MI" => "Michigan",
        "MN" => "Minnesota",
        "MS" => "Mississippi",
        "MO" => "Missouri",
        "MT" => "Montana",
        "NE" => "Nebraska",
        "NV" => "Nevada",
        "NH" => "New Hampshire",
        "NJ" => "New Jersey",
        "NM" => "New Mexico",
        "NY" => "New York",
        "NC" => "North Carolina",
        "ND" => "North Dakota",
        "OH" => "Ohio",
        "OK" => "Oklahoma",
        "OR" => "Oregon",
        "PA" => "Pennsylvania",
        "RI" => "Rhode Island",
        "SC" => "South Carolina",
        "SD" => "South Dakota",
        "TN" => "Tennessee",
        "TX" => "Texas",
        "UT" => "Utah",
        "VT" => "Vermont",
        "VA" => "Virginia",
        "WA" => "Washington",
        "WV" => "West Virginia",
        "WI" => "Wisconsin",
        "WY" => "Wyoming",
        "DC" => "Washington DC",
        _ => panic!("Invalid state abbreviation: {:?}", abbreviation)
    }
}

fn read_records<R: DeserializeOwned + Into<Record>>(file: &str) -> Vec<Record> {
    csv::Reader::from_reader(File::open(file).unwrap())
        .deserialize::<R>()
        .map(|record| record.unwrap().into())
        .collect()
}

fn read_records_2016() -> Vec<Record> {
    read_records::<Record2016>("../2016_US_County_Level_Presidential_results.csv")
}

fn read_records_2020() -> Vec<Record> {
    read_records::<Record2020>("../2020_US_County_Level_Presidential_results.csv")
}

#[derive(Debug, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
struct StateCounty {
    state: String,
    county: String,
}

impl StateCounty {
    pub fn pretty(&self) -> String {
        format!("{}, {}", self.state, self.county)
    }
}

#[derive(Debug, Clone)]
struct Votes {
    dem: i64,
    rep: i64,
}

impl Votes {
    pub fn percentage_dem(&self) -> f64 {
        self.dem as f64 / (self.dem as f64 + self.rep as f64)
    }

    pub fn total(&self) -> i64 {
        self.dem + self.rep
    }
}

fn prepare_map(records: impl IntoIterator<Item=Record>) -> BTreeMap<StateCounty, Option<Votes>> {
    let mut map = BTreeMap::new();
    for record in records {
        let Record {
            state,
            county,
            votes_dem,
            votes_rep,
        } = record;
        if votes_dem == 0 || votes_rep == 0 {
            println!("Bad data, zero votes: {:?}", StateCounty { state, county });
            continue;
        }
        match map.entry(StateCounty { state, county }) {
            Entry::Occupied(mut entry) => {
                entry.insert(None);
                println!("Bad data, duplicate key {:?}", entry.key());
            },
            Entry::Vacant(entry) => {
                entry.insert(Some(Votes {
                    dem: votes_dem,
                    rep: votes_rep,
                }));
            },
        };
    }
    map
}

#[derive(Debug, Clone)]
struct VotesByYear {
    year_2016: Votes,
    year_2020: Votes,
}

fn combine_maps(
    map_2016: BTreeMap<StateCounty, Option<Votes>>,
    map_2020: BTreeMap<StateCounty, Option<Votes>>,
) -> BTreeMap<StateCounty, VotesByYear> {
    let mut combined = BTreeMap::new();
    for key in map_2016.keys().chain(map_2020.keys()) {
        match (map_2016.get(key), map_2020.get(key)) {
            (Some(&Some(ref year_2016)), Some(&Some(ref year_2020))) => {
                combined.insert(key.clone(), VotesByYear {
                    year_2016: year_2016.clone(),
                    year_2020: year_2020.clone(),
                });
            },
            (Some(&Some(_)), _) => {
                println!("Bad data, county present in 2016 but not 2020: {:?}", key);
            },
            (_, Some(&Some(_))) => {
                println!("Bad data, county present in 2020 but not 2016: {:?}", key);
            },
            _ => {},
        };
    }
    combined
}

fn compute_trends(votes_map: &BTreeMap<StateCounty, VotesByYear>) -> BTreeMap<StateCounty, f64> {
    let mut trend_map = BTreeMap::new();
    for (key, by_year) in votes_map {
        let trend = by_year.year_2020.percentage_dem() - by_year.year_2016.percentage_dem();
        trend_map.insert(key.clone(), trend);
    }
    trend_map
}

fn pretty_print_votes(votes_map: &BTreeMap<StateCounty, VotesByYear>) {
    println!("\n==== printing votes ====\n");
    println!("");
    println!("{: <40}|{: <14}|{: <14}|{: <14}|{: <14}", 
        "County", 
        "Biden 2020",
        "Trump 2020",
        "Clinton 2016",
        "Trump 2016",
    );
    for (key, by_year) in votes_map {
        println!("{: <40}|{: <14}|{: <14}|{: <14}|{: <14}", 
            key.pretty(), 
            by_year.year_2020.dem,
            by_year.year_2020.rep,
            by_year.year_2016.dem,
            by_year.year_2016.rep,
        );
    }
}

fn pretty_print_trends(trends: &BTreeMap<StateCounty, f64>) {
    println!("\n==== printing trends ====\n");
    let mut num_trend_dem = 0;
    let mut num_trend_rep = 0;
    for (key, trend) in trends {
        let mut trend = *trend;
        let trend_dir = if trend < 0.0 {
            trend *= -1.0;
            num_trend_rep += 1;
            "           REPUBLICAN"
        } else {
            num_trend_dem += 1;
            "DEMOCRAT"
        };
        println!("{: <40} {: <10.1}% {}", key.pretty(), trend * 100.0, trend_dir)
    }
    println!("\n\n");
    println!("{} counties trended Republican", num_trend_rep);
    println!("{} counties trended Democrat", num_trend_dem);
}

fn print_county_victory_tally(votes_maps: &BTreeMap<StateCounty, VotesByYear>) {
    println!("\n==== printing county victory tallys ====\n");
    println!("in 2016, {} counties voted Republican", {
        votes_maps
            .iter()
            .filter(|&(_, votes)| votes.year_2016.rep > votes.year_2016.dem)
            .count()
    });
    println!("in 2016, {} counties voted Democrat", {
        votes_maps
            .iter()
            .filter(|&(_, votes)| votes.year_2016.dem > votes.year_2016.rep)
            .count()
    });
    println!("in 2020, {} counties voted Republican", {
        votes_maps
            .iter()
            .filter(|&(_, votes)| votes.year_2020.rep > votes.year_2020.dem)
            .count()
    });
    println!("in 2020, {} counties voted Democrat", {
        votes_maps
            .iter()
            .filter(|&(_, votes)| votes.year_2020.dem > votes.year_2020.rep)
            .count()
    });
}

fn print_most_extreme_trends(
    trends: &BTreeMap<StateCounty, f64>,
    votes_maps: &BTreeMap<StateCounty, VotesByYear>,
) {
    println!("\n==== printing most extreme trends ====\n");

    let mut trends_vec: Vec<(&StateCounty, &f64)> = trends.iter().collect();
    //trends_vec.sort_by_key(|&(_, &trend)| -trend.abs());
    trends_vec.sort_by(|&(_, &a), &(_, &b)| {
        b.abs().partial_cmp(&a.abs()).unwrap()
    });
    for (i, &(key, _)) in trends_vec
        .iter()
        .filter(|&(key, _)| 
            votes_maps[key].year_2016.total() >= 632103
            || votes_maps[key].year_2020.total() >= 632103)
        .enumerate()
    {
        let mut trend = trends[key];
        let trend_dir = if trend < 0.0 {
            trend *= -1.0;
            "           REPUBLICAN"
        } else {
            "DEMOCRAT             "
        };
        println!("{: <2}. {: <40} {: >4.1}% {}  TOTAL_16={: >8.1}k   TOTAL_20={: >8.1}k", 
            i, 
            key.pretty(), 
            trend * 100.0, 
            trend_dir,
            votes_maps[key].year_2016.total() as f64 / 1000.0,
            votes_maps[key].year_2020.total() as f64 / 1000.0,
        );
        if key.pretty() == "Michigan, Oakland County" { break; }
    }
}

fn main() {
    let votes_maps = combine_maps(
        prepare_map(read_records_2016()),
        prepare_map(read_records_2020()),
    );
    let trends = compute_trends(&votes_maps);
    print_most_extreme_trends(&trends, &votes_maps);
    //print_county_victory_tally(&votes_maps);
    //pretty_print_votes(&votes_maps);
    //pretty_print_trends(&trends);
}
